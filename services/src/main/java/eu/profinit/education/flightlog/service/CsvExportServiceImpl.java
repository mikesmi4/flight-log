package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.domain.entities.Flight;
import eu.profinit.education.flightlog.exceptions.FlightLogException;
import org.apache.commons.csv.CSVFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.to.FileExportTo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.apache.commons.csv.CSVPrinter;

@Service
public class CsvExportServiceImpl implements CsvExportService {
    // TODO 4.4 Vhodné vlastnosti CSV souboru definujte jako konstanty
    private enum Header {
        FlightID,TakeoffTime,LandingTime,Immatriculation,Type,Pilot,Copilot,Task,TowplaneID,GliderID
    }

    private static final String DATE_PATTERN = "dd.MM.yyyy HH:mm:ss";

    private static final char delimiter = ',';

    private final FlightRepository flightRepository;

    private final String fileName;

    public CsvExportServiceImpl(FlightRepository flightRepository, @Value("${csv.export.flight.fileName}") String fileName) {
        this.flightRepository = flightRepository;
        this.fileName = fileName;
    }

    private String formatDateTime(LocalDateTime dateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        return formatter.format(dateTime);
    }

    @Override
    public FileExportTo getAllFlightsAsCsv() {
        // TODO 4.3: Naimplementujte vytváření CSV.
        // Tip: můžete použít Apache Commons CSV - https://commons.apache.org/proper/commons-csv/ v příslušných pom.xml naleznete další komentáře s postupem
        try (
            ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
            Writer printWriter = new OutputStreamWriter(byteOutputStream, StandardCharsets.UTF_8);
            CSVPrinter csvExport = new CSVPrinter(printWriter, CSVFormat.DEFAULT.builder()
                .setHeaderComments("\"sep=,\"")
                .setDelimiter(delimiter)
                .setRecordSeparator('\n')
                .setNullString("")
                .setHeader(Header.class)
                .build());
        ) {
            // TODO: print data to CSV
            List<Flight> flights = flightRepository.findAll(Sort.by(Sort.Order.asc("takeoffTime"), Sort.Order.asc("id")));

            for(Flight f : flights) {
                csvExport.printRecord(
                    f.getId().getId(),
                    formatDateTime(f.getTakeoffTime()),
                    f.getLandingTime() != null ? formatDateTime(f.getLandingTime()) : "",
                    f.getAirplane().getSafeImmatriculation(),
                    f.getAirplane().getSafeType(),
                    f.getPilot().getFullName(),
                    f.getCopilot() != null ? f.getCopilot().getFullName() : "",
                    f.getTask() != null ? f.getTask().getValue() : "",
                    f.getTowplaneFlight() != null ? f.getTowplaneFlight().getId().getId() : "",
                    f.getGliderFlight() != null ? f.getGliderFlight().getId().getId() : ""
                );
            }
            csvExport.flush();
            // TODO: get data from byteOutputStream using toByteArray()
            return new FileExportTo(fileName, new MediaType("text", "csv"), StandardCharsets.UTF_8.name(), byteOutputStream.toByteArray());
        } catch (IOException exception) {
            throw new FlightLogException("Error during flights CSV export", exception);
        }
    }
}