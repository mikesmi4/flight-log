package eu.profinit.education.flightlog.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import eu.profinit.education.flightlog.AbstractIntegrationTest;
import eu.profinit.education.flightlog.to.FileExportTo;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CsvExportServiceTest extends AbstractIntegrationTest {

    @Autowired
    private CsvExportService testSubject;

    private String readFileToString(String fileName) throws IOException, URISyntaxException {
        return new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(fileName).toURI())));
    }

    // TODO 6.1: Odstrante anotaci @Disabled, aby se test vykonaval
    @Test
    public void testCSVExport() throws IOException, URISyntaxException {
        FileExportTo fileExportTo = testSubject.getAllFlightsAsCsv();

        // TODO 6.2: zkontrolujte obsah CSV
        // Tip: Využijte třídu FileUtils
        String actual = new String(fileExportTo.getContent(), fileExportTo.getEncoding());
        String expected = readFileToString("expectedExport.csv");
        assertEquals(expected, actual, "File contents are not equal");
    }


}