package eu.profinit.education.flightlog.rest;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.profinit.education.flightlog.service.AirplaneService;
import eu.profinit.education.flightlog.to.AirplaneTo;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class AirplaneController {

    private final AirplaneService airplaneService;

    // TODO 3.1: Vystavte REST endpoint vracející seznam klubových letadel
    // Tip: letadla získáte voláním AirplaneService
    // Tip: bude se volat metoda GET na /airplane
    // Tip: struktura odpovědi je dána objektem AirplaneTo
    @GetMapping("/airplane")
    public List<AirplaneTo> getAirplanes() {
        return airplaneService.getClubAirplanes();
    }
}
